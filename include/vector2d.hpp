
template <class T> class Vector2D {
public:

    /**
     * Dot product
     * 
     * @param other vector to dot with this
     * @return dot product of this and other
     */
    static T dot(const Vector2D<T>& v1, const Vector2D<T>& v2);

    /**
     * Default Vector2D constructor
     * Sets x and y to 0
     */
    Vector2D();
    
    /**
     * Vector2D constructor
     * @param x
     * @param y
     */
    Vector2D(T x, T y);

    // Default copy semantics
    Vector2D(const Vector2D& other) = default;
    Vector2D& operator=(const Vector2D& other) = default;

    // Default move semantics
    Vector2D(Vector2D&& other) = default;
    Vector2D& operator=(Vector2D&& other) = default;

    // Default destructor
    ~Vector2D() = default;

    T getX() const;
    T getY() const;

    T setX(T x);
    T setY(T y);

    /**
     * Euclidian length of the vector
     * 
     * @return length of the vector
     */
    T getLength() const;

    T getLengthSquared() const;

    /**
     * Normalized vector
     * 
     * @return vector of the same direction of length 1
     */
    Vector2D normalized() const;

    /**
     * Perpendicular vector
     * 
     * @return vector of the same length which is perpindicular to the current vector
     */
    Vector2D perpindicular() const;

    /**
     * Element-wise addition
     * 
     * @param other second vector in the addition
     * @return vector equal to this plus other
     */
    Vector2D operator+(const Vector2D& other) const;

    /**
     * Element-wise subtraction
     * 
     * @param other second vector in the subtraction
     * @return vector equal to this minus other
     */
    Vector2D operator-(const Vector2D& other) const;

    /**
     * Multiplication by a scalar
     * 
     * @param l scalar to multiply by
     * @return this multiplied by l
     */
    Vector2D operator*(T l) const;

    /**
     * Division by a scalar
     * 
     * @param l scalar to divide by
     * @return this divided by l
     */
    Vector2D operator/(T l) const;

    /**
     * Scalar projection of a vector onto this
     * 
     * @param point the vector to project
     * @return scalar projection of point onto this
     */
    T scalarProjectionOf(const Vector2D& point) const;

    /**
     * Vector projection of a vector onto this
     * 
     * @param point the vector to project
     * @return vector projection of point onto this
     */
    Vector2D projectionOf(const Vector2D& point) const;

private:
    T x;
    T y;

};