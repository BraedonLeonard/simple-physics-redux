#include "vector2d.hpp"
#include <vector>

template <class T>
class ConvexPolygon2D {
public:

    // Default constructor/destructor
    ConvexPolygon2D() = default;
    ~ConvexPolygon2D() = default;

    // Default copy semantics
    ConvexPolygon2D(const ConvexPolygon2D<T>& other) = default;
    ConvexPolygon2D& operator=(const ConvexPolygon2D<T>& other) = default;

    // Default move semantics
    ConvexPolygon2D(ConvexPolygon2D<T>&& other) = default;
    ConvexPolygon2D& operator=(ConvexPolygon2D<T>&& other) = default;

    /**
     * ConvexPolygon2D constructor
     * Assigns Vector2Ds via copy construction
     * 
     * @param Vector2Ds Vector2Ds to be copied
     */
    ConvexPolygon2D(const std::vector<Vector2D<T>>& Vector2Ds);

    /**
     * ConvexPolygon2D constructor
     * Assigns Vector2Ds via move construction
     * 
     * @param Vector2Ds Vector2Ds to be moved
     */
    ConvexPolygon2D(std::vector<Vector2D<T>>&& Vector2Ds);

    /**
     * Vector2Ds accessor
     * @return ordered list of polygon Vector2Ds
     */
    const std::vector<Vector2D<T>>& getVertices() const;

    /**
     * Vector2D<T> accessor
     * Accesses the i-th Vector2D<T> of the polygon
     * 
     * @param i index to access
     * @throw std::out_of_range index is out of range
     */
    const Vector2D<T>& getVertex(int i) const;

    /**
     * Tests polygon-to-polygon collision using Seperating Axis Theorem
     * 
     * @param poly convex polygon to check collision with
     */
    bool collidesWith(const ConvexPolygon2D<T>& poly) const;

private:
    /**
     * Ordered list of polygon's Vector2Ds
     * Each edge connects from a Vector2D<T> to the next Vector2D<T> in the list. The final Vector2D<T> is considered
     * to be connected to the first Vector2D<T>, so the connecting Vector2D<T> should not be repeated.
     */
    std::vector<Vector2D<T>> _vertices;

};