#include "vector2d.hpp"

template <class T>
class Line2D {
public:

    /**
     * Line2D constructor
     * Constructs a line based on an origin vector and a direction vector
     * 
     * @param origin origin of the line
     * @param direction direction of the line
     */
    Line2D(Vector2D<T> origin, Vector2D<T> direction);

    /**
     * Checks if a point is a part of the line
     * 
     * @param point point to be checked
     * @return true if the line contains the point, false otherwise
     */
    bool contains(const Vector2D<T>& point) const;

    /**
     * Projects a vector onto the line
     * 
     * @param point point to be projected
     * @return projected point
     */
    Vector2D<T> projectionOf(const Vector2D<T>& point) const;

    /**
     * Tranforms a point into a point on the line
     * The point is projected onto the line, and returned as a multiple of direction
     * vectors away from the origin it is.
     * 
     * @param point point to be transformed
     * @return transformed point
     */
    float projectionTransformOf(const Vector2D<T>& point) const;

private:
    Vector2D<T> origin;
    Vector2D<T> direction;
};