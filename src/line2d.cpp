#include "line2d.hpp"

template <class T>
Line2D<T>::Line2D<T>(Vector2D<T> origin, Vector2D<T> direction) 
    : origin(origin), direction(direction) {}

template <class T>
bool Line2D<T>::contains(const Vector2D<T>& point) const {
    Vector2D<T> translated = point - this->origin;
    return translated.getX()/this->direction.getX() 
        == translated.getY()/this->direction.getY();
}

template <class T>
Vector2D<T> Line2D<T>::projectionOf(const Vector2D<T>& point) const {
    return direction.projectionOf(point - origin) + origin;
}

template <class T>
float Line2D<T>::projectionTransformOf(const Vector2D<T>& point) const {
    return direction.scalarProjectionOf(point - origin);
}