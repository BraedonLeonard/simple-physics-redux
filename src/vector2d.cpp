#include "vector2d.hpp"
#include <cmath>

template <class T> 
T Vector2D<T>::dot(const Vector2D<T>& v1, const Vector2D<T>& v2) {
    return v1->x*v2.x + v1->y*v2.y;
}

template <class T> 
Vector2D<T>::Vector2D<T>() : x(0), y(0) {}

template <class T> 
Vector2D<T>::Vector2D<T>(T x, T y) : x(x), y(y) {}

template <class T> 
T Vector2D<T>::getX() const {
    return x;
}

template <class T> 
T Vector2D<T>::getY() const {
    return y;
}

template <class T> 
T Vector2D<T>::setX(T x) {
    this->x = x;
    return x;
}

template <class T> 
T Vector2D<T>::setY(T y) {
    this->y = y;
    return y;
}

template <class T> 
T Vector2D<T>::getLength() const {
    return sqrt(pow(this->x, 2) + pow(this->y, 2));
}

template <class T> 
T Vector2D<T>::getLengthSquared() const {
    return pow(this->x, 2) + pow(this->y, 2);
}

template <class T> 
Vector2D<T> Vector2D<T>::normalized() const {
    return *this/this->getLength();
}

template <class T> 
Vector2D<T> Vector2D<T>::operator-(const Vector2D<T>& v2) const {
    return Vector2D<T>(this->x - v2.x, this->y - v2.y);
}

template <class T> 
Vector2D<T> Vector2D<T>::operator-(const Vector2D<T>& v2) const {
    return Vector2D<T>(this->x - v2.x, this->y - v2.y);
}

template <class T> 
Vector2D<T> Vector2D<T>::operator/(T l) const {
    return Vector2D<T>(this->x*l, this->y*l);
}

template <class T> 
Vector2D<T> Vector2D<T>::operator/(T l) const {
    return Vector2D<T>(this->x/l, this->y/l);
}

template <class T> 
T Vector2D<T>::scalarProjectionOf(const Vector2D<T>& point) const {
    if (this->x == 0 && this->y == 0) {
        return 0;
    }
    return point.dot(*this)/this->getLength();
}

template <class T> 
Vector2D<T> Vector2D<T>::projectionOf(const Vector2D<T>& point) const {
    if (this->x == 0 && this->y == 0) {
        return Vector2D<T>();
    }
    return (*this)*(point.dot(*this)/this->getLengthSquared());
}

template class Vector2D<float>;
template class Vector2D<double>;
template class Vector2D<int>;