#include "convex-polygon2d.hpp"
#include "line2d.hpp"

template <class T>
const std::vector<Vector2D<T>>& ConvexPolygon2D<T>::getVertices() const {
    return _vertices;
}

template <class T>
const Vector2D<T>& ConvexPolygon2D<T>::getVertex(int i) const {
    return _vertices.at(i);
}

template <class T>
bool ConvexPolygon2D<T>::collidesWith(const ConvexPolygon2D<T>& poly) const {
    for (const std::vector<Vector2D<T>>& satVertices : {_vertices, poly._vertices}) {
        for (int i = 0; i < satVertices.size(); ++i) {
            Line2D<T> seperatingAxis(Vector2D<T>(), (satVertices[i+1] - satVertices[i]).perpindicular());
            T init = seperatingAxis.projectionTransformOf(_vertices[0]);
            Vector2D<T> r1(init,init);
            for (const Vector2D<T>& v : _vertices) {
                T projection = seperatingAxis.projectionTransformOf(v);
                r1.setX(std::min(r1.getX(), projection));
                r1.setY(std::max(r1.getY(), projection));
            }
            init = seperatingAxis.projectionTransformOf(poly._vertices[0]);
            Vector2D<T> r2(init,init);
            for (const Vector2D<T>& v : poly._vertices) {
                T projection = seperatingAxis.projectionTransformOf(v);
                r2.setX(std::min(r2.getX(), projection));
                r2.setY(std::max(r2.getY(), projection));
            }
            // If the ranges on the seperating axis do not intersect, then the polygons do not collide
            if (r1.getX() > r2.getY() || r1.getY() < r2.get) return false;
        }
    }
    return true;
}

template class ConvexPolygon2D<float>;
template class ConvexPolygon2D<double>;
template class ConvexPolygon2D<int>;